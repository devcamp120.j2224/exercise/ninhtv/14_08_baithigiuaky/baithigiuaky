package com.devcamp.rest_api.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.rest_api.model.Album;
import com.devcamp.rest_api.model.Image;
import com.devcamp.rest_api.repository.albumRepository;
import com.devcamp.rest_api.repository.imageRepository;

@RequestMapping("/")
@CrossOrigin
@RestController
public class apiController {
    @Autowired
    imageRepository imageRepository;
    @Autowired
    albumRepository albumRepository;

    @GetMapping("/album/all")
    public ResponseEntity<List<Album>> getAllAlbum() {
        try {
            List<Album> lisAlbums = new ArrayList<Album>();
            albumRepository.findAll().forEach(lisAlbums::add);
            return new ResponseEntity<>(lisAlbums, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/image/all")
    public ResponseEntity<List<Image>> getAllImage() {
        try {
            List<Image> lisImages = new ArrayList<Image>();
            imageRepository.findAll().forEach(lisImages::add);
            return new ResponseEntity<>(lisImages, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/album/detail/{id}")
    public ResponseEntity<Album> getAlbumById(@PathVariable("id") int id) {
        Optional<Album> album = albumRepository.findById(id);
        if (album.isPresent()) {
            return new ResponseEntity<>(album.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/image/detail/{id}")
    public ResponseEntity<Image> getImageById(@PathVariable("id") int id) {
        Optional<Image> image = imageRepository.findById(id);
        if (image.isPresent()) {
            return new ResponseEntity<>(image.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/album/create")
    public ResponseEntity<Object> createAlbum(@Valid @RequestBody Album album) {
        Optional<Album> findIdByAlbum = albumRepository.findById(album.getId());
        try {

            if (findIdByAlbum.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" album already exsit ");
            }

            album.setNgayTao(new Date());
            album.setTenAlbum(album.getTenAlbum());
            album.setMaAlbum(album.getMaAlbum());
            album.setMoTa(album.getMoTa());
            album.setImages(album.getImages());
            Album createNewAlbum = albumRepository.save(album);
            return new ResponseEntity<>(createNewAlbum, HttpStatus.CREATED);
        } catch (Exception e) {

            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified album: " +
                            e.getCause().getCause().getMessage());
        }

    }

    @PostMapping("/image/create/{id}")
    public ResponseEntity<Object> createImage(@PathVariable("id") int id, @Valid @RequestBody Image image) {
        Optional<Album> findIdByAlbum = albumRepository.findById(id);
        Optional<Image> createImageById = imageRepository.findById(image.getId());
        try {
            if (findIdByAlbum.isPresent()) {
                if (createImageById.isPresent()) {
                    return ResponseEntity.unprocessableEntity().body(" image already exsit ");
                }
                Album album = findIdByAlbum.get();
                image.setNgayTao(new Date());
                image.setMoTa(image.getMoTa());
                image.setLinkAnh(image.getLinkAnh());
                image.setTenAnh(image.getTenAnh());
                image.setAlbum(album);

                Image createNewImage = imageRepository.save(image);
                return new ResponseEntity<>(createNewImage, HttpStatus.CREATED);
            } else {
                return null;
            }
        } catch (Exception e) {

            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified image: " +
                            e.getCause().getCause().getMessage());
        }

    }

    @PutMapping("/album/update/{id}") // Dùng phương thức PUT
    public ResponseEntity<Object> updateAlbumById(@PathVariable("id") int id, @Valid @RequestBody Album album) {
        Optional<Album> findIdByAlbum = albumRepository.findById(id);

        try {
            if (findIdByAlbum.isPresent()) {
                Album albumNew = findIdByAlbum.get();
                albumNew.setImages(album.getImages());
                albumNew.setMoTa(album.getMoTa());
                albumNew.setTenAlbum(album.getTenAlbum());
                albumNew.setMaAlbum(album.getMaAlbum());
                Album albumUpdate = albumRepository.save(albumNew);
                return new ResponseEntity<>(albumUpdate, HttpStatus.OK);
            } else {
                return ResponseEntity.badRequest().body("Failed to get specified Album: " + id + "  for update.");
            }

        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Album:" + e.getCause().getCause().getMessage());
        }

    }

    @PutMapping("/image/update/{id}") // Dùng phương thức PUT
    public ResponseEntity<Object> updateImageById(@PathVariable("id") int id, @Valid @RequestBody Image image) {
        Optional<Image> findIdByImage = imageRepository.findById(id);

        try {
            if (findIdByImage.isPresent()) {
                Image imageNew = findIdByImage.get();
                imageNew.setLinkAnh(image.getLinkAnh());
                imageNew.setMoTa(image.getMoTa());
                imageNew.setTenAnh(image.getTenAnh());
                Image imageUpdate = imageRepository.save(imageNew);
                return new ResponseEntity<>(imageUpdate, HttpStatus.OK);
            } else {
                return ResponseEntity.badRequest().body("Failed to get specified Image: " + id + "  for update.");
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Image:" + e.getCause().getCause().getMessage());
        }

    }

    @DeleteMapping("/album/delete/{id}") // Dùng phương thức DELETE
    public ResponseEntity<Album> deleteAlbumById(@PathVariable("id") int id) {
        try {
            albumRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/image/delete/{id}") // Dùng phương thức DELETE
    public ResponseEntity<Album> deleteImageById(@PathVariable("id") int id) {
        try {
            imageRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
