package com.devcamp.rest_api.model;

import java.util.*;

import javax.persistence.CascadeType;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "albums")
public class Album {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private int id;

    @NotEmpty(message = "không được để trống mã album")
    @Length(min = 3, max = 10, message = "ít nhất 3 ký tự , tối đa là 10")
    // @Column(unique = true)
    private String maAlbum;

    // @Column(name = "ten_album")
    @NotEmpty(message = "không được để trống tên album")
    private String tenAlbum;

    // @Column(name = "mo_ta_album")
    @Length(min = 3, max = 100, message = "tối đa 200 ký tự")
    private String moTa;

    // @Column(name = "ngay_tao_album")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "MM/dd/yyyy")
    @CreatedDate
    private Date ngayTao;

    @OneToMany(targetEntity = Image.class, cascade = CascadeType.ALL)

    @JoinColumn(name = "album_id")
    private List<Image> images;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTenAlbum() {
        return tenAlbum;
    }

    public void setTenAlbum(String tenAlbum) {
        this.tenAlbum = tenAlbum;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public Date getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }

    public List<Image> getImages() {
        return images;
    }

    public Album(int id, String maAlbum, String tenAlbum, String moTa, Date ngayTao, List<Image> images) {
        this.id = id;
        this.maAlbum = maAlbum;
        this.tenAlbum = tenAlbum;
        this.moTa = moTa;
        this.ngayTao = ngayTao;
        this.images = images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public Album() {
    }

    public Album(int id, String tenAlbum, String moTa, Date ngayTao, List<Image> images) {
        this.id = id;
        this.tenAlbum = tenAlbum;
        this.moTa = moTa;
        this.ngayTao = ngayTao;
        this.images = images;
    }

    public String getMaAlbum() {
        return maAlbum;
    }

    public void setMaAlbum(String maAlbum) {
        this.maAlbum = maAlbum;
    }

}
