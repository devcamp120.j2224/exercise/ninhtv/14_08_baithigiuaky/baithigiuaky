package com.devcamp.rest_api.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "images")
public class Image {
    // mã ảnh, tên ảnh, link ảnh, mô tả, ngày tạo
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true)
    private int id;

    // @Column(name = "ten_anh")
    @NotEmpty(message = "không được để trống tên ảnh")
    private String tenAnh;

    // @Column(name = "linh_anh")
    @NotEmpty(message = "không được để trống link ảnh")
    private String linkAnh;

    // @Column(name = "mo_ta_anh")
    @Length(min = 3, max = 100, message = "tối đa 200 ký tự")
    private String moTa;

    // @Column(name = "ngay_tao_anh")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "MM/dd/yyyy")
    @CreatedDate
    private Date ngayTao;

    @ManyToOne
    @JsonIgnore
    private Album album;

    public Image() {
    }

    public Image(int id, String tenAnh, String linkAnh, String moTa, Date ngayTao, Album album) {
        this.id = id;
        this.tenAnh = tenAnh;
        this.linkAnh = linkAnh;
        this.moTa = moTa;
        this.ngayTao = ngayTao;
        this.album = album;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTenAnh() {
        return tenAnh;
    }

    public void setTenAnh(String tenAnh) {
        this.tenAnh = tenAnh;
    }

    public String getLinkAnh() {
        return linkAnh;
    }

    public void setLinkAnh(String linkAnh) {
        this.linkAnh = linkAnh;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public Date getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

}
