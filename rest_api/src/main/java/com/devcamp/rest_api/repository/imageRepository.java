package com.devcamp.rest_api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.rest_api.model.Image;

public interface imageRepository extends JpaRepository<Image, Integer> {

}
