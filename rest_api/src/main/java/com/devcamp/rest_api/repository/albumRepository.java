package com.devcamp.rest_api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.rest_api.model.Album;

public interface albumRepository extends JpaRepository<Album, Integer> {

}
